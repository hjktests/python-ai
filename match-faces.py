import face_recognition

# Face comparing fucntion
# known = known face location
# unknown = unknown face location
# name = the persons name


def compare_images(known, unknown, name):
    # Load known face and get encoding
    known_face = face_recognition.load_image_file(known)
    known_face_encoding = face_recognition.face_encodings(known_face)[0]

    # Load unknown face and get encoding
    unknown_face = face_recognition.load_image_file(unknown)
    unknown_face_encoding = face_recognition.face_encodings(unknown_face)[0]

    # Compare faces
    result = face_recognition.compare_faces(
        [known_face_encoding], unknown_face_encoding)

    if result[0]:
        print(f'This is {name}')
    else:
        print(f'This is not {name}')


# Compare Bill Gates to himself
compare_images('./img/known/Bill Gates.jpg',
               './img/unknown/bill-gates.jpg', "Bill Gates")
# Compare Donald Trump to Bill Gates
compare_images('./img/known/Bill Gates.jpg',
               './img/unknown/d-trump.jpg', "Bill Gates")

# Output:
# python match-faces.py
# This is Bill Gates
# This is not Bill Gates
