import face_recognition
from PIL import Image, ImageDraw

# Load images

bill_gates_image = face_recognition.load_image_file(
    './img/known/Bill Gates.jpg')
bill_gates_encoding = face_recognition.face_encodings(bill_gates_image)[0]

steve_jobs_image = face_recognition.load_image_file(
    './img/known/Steve Jobs.jpg')
steve_jobs_encoding = face_recognition.face_encodings(steve_jobs_image)[0]

# Create an array of encodings and names
known_face_encodings = [
    bill_gates_encoding,
    steve_jobs_encoding
]
known_face_names = [
    "Bill Gates",
    "Steve Jobs"
]

# Load test image
test_image = face_recognition.load_image_file(
    './img/groups/bill-steve.jpg')

# Find faces in test image
face_locations = face_recognition.face_locations(test_image)
face_encodings = face_recognition.face_encodings(test_image, face_locations)

# Convert to PIL format
pil_image = Image.fromarray(test_image)

# Create an ImageDraw instance
draw = ImageDraw.Draw(pil_image)

# Loop through test_image faces
for(top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
    matches = face_recognition.compare_faces(
        known_face_encodings, face_encoding)

    name = "Unknown person"

    # If known face is found
    if True in matches:
        first_match_index = matches.index(True)
        name = known_face_names[first_match_index]

    # Draw box around the face
    draw.rectangle(((left, top), (right, bottom)), outline=(255, 0, 0))

    # Draw label
    text_width, text_height = draw.textsize(name)
    draw.rectangle(((left, bottom - text_height - 5),
                    (right, bottom)), fill=(0, 0, 0), outline=(255, 0, 0))
    draw.text((left + 6, bottom - text_height - 5),
              name, fill=(255, 255, 255, 255))

# Delete draw instance from memory
del draw

# Display image
pil_image.save('./img/identified/bill-steve.jpg')
