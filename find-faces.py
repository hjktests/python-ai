import sys
import face_recognition

# Select the image file
# Arguments must be 1 or 2 so the correct image will load
image = face_recognition.load_image_file(f'./img/groups/team{sys.argv[1]}.jpg')
# Find the face locations from the image
face_locations = face_recognition.face_locations(image)

# Print the array of coordinates of each face
print(face_locations)

# Amount of people in the image
print(f'There are {len(face_locations)} people in this image.')
